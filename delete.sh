sudo docker container stop $(docker container ls -a -q)
sudo docker container rm $(docker container ls -a -q) --force
sudo docker service rm $(docker service ls)
sudo docker image rm $(docker image ls -a -q) --force
sudo docker volume rm $(docker volume ls -q) --force
