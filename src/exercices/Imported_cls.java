import se.miun.rakr1800.dt062g.jpaint.Circle_pac;
import se.miun.rakr1800.dt062g.jpaint.Rectangle_pac;


public class Imported_cls
{
    public static void main(String[] args)
    {
        Circle_pac p = new Circle_pac(2.0);
        p.print_circle();

        int a = 1;
        System.out.println("\n" + "O - before - a: " + a);
        Circle_pac.inc(a);
        System.out.println("O - after - a: " + a + "\n");

        p.x = 3;
        System.out.println("O - before - a: " + p.x);
        Circle_pac.change(p);
        System.out.println("O - after - a: " + p.x + "\n");

        p.x = 5;
        System.out.println("O - before - a: " + p.x);
        Circle_pac.change_var(p);
        System.out.println("O - after - a: " + p.x + "\n");

        p.x = 7;
        System.out.println("O - before - a: " + p.x);
        p.change_var_this();
        System.out.println("O - after - a: " + p.x + "\n");

        Circle_pac obj[] = new Circle_pac[5];

        obj[0] = new Circle_pac(3.0);
        obj[1] = new Circle_pac(2.0);
        obj[3] = new Circle_pac(4.0);
        obj[2] = new Circle_pac(1.0);
        obj[4] = new Circle_pac(0.0);

        for (int i = 0; i < obj.length; i++)
        {
            if (obj[i] != null)
            {
                obj[i].print_circle();
            }
        }

        System.out.println("\n");

        for (int i = 0; i < obj.length; i++)
        {
            for (int j = 0; j < obj.length-i-1; j++)
            {
                if (obj[i] != null && obj[j] != null)
                {
                    if (obj[j].radius > obj[j+1].radius)
                    {
                        Circle_pac temp_obj = obj[j];
                        obj[j] = obj[j+1];
                        obj[j+1] = temp_obj;
                    }
                }
            }
        }

        for (int i = 0; i < obj.length; i++)
        {
            if (obj[i] != null)
            {
                obj[i].print_circle();
            }
        }
    }
}
