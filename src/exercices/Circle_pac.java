package se.miun.rakr1800.dt062g.jpaint;


public class Circle_pac
{
    public double radius;
    private double circumference;
    private double area;

    public int x;

    // Constructor
    public Circle_pac(double aRadius)
    {
        this.radius = aRadius;
    }

    public void calc_area()
    {
        this.area = this.radius;
    }

    public void print_circle()
    {
        System.out.println("radius: " + this.radius);
        System.out.println("circumference: " + this.circumference);
        System.out.println("area: " + this.area);
    }

    public static void inc(int a)
    {
        System.out.println("I - before - a: " + a);
        a++;
        System.out.println("I - after - a: " + a);
    }

    public static void change(Circle_pac w)
    {
        System.out.println("I - before - w.x: " + w.x);
        w = new Circle_pac(2.0);
        w.x = 4;
        System.out.println("I - after - w.x: " + w.x);
    }

    public static void change_var(Circle_pac p)
    {
        System.out.println("I - before - p.x: " + p.x);
        p.x = 6;
        System.out.println("I - after - p.x: " + p.x);
    }

    public void change_var_this()
    {
        System.out.println("I - before - p.x: " + this.x);
        this.x = 8;
        System.out.println("I - after - p.x: " + this.x);
    }
}
