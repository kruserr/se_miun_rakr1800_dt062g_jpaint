package se.miun.rakr1800.dt062g.jpaint;


public class Rectangle_pac
{
    private int number;

    // Constructor
    public Rectangle_pac(int number)
    {
        this.number = number;
    }

    // Add number
    public void add(int aNumber)
    {
        this.number += aNumber;
    }

    // Print number
    public int print_number()
    {
        return this.number;
    }
}
