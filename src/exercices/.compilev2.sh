#!/bin/bash

# Compile all packages with in the current directory
#find . -type f -name "*_pac.java" -exec javac -d . {} \;

# Compile all classes in the current directory
#find . -type f -name "*_cls.java" -exec javac {} \;

# Get arr of classes
arr=($(ls -d *.java | grep -oP '.*?(?=\.)'))
arr_len=${#arr[*]}

# Run all classes
for (( i = 0; i < $arr_len; i++ ))
do
    #java ${arr[$i]}
    pac=($(cat ${arr[$i]}.java | grep "Test"))
done

echo ${pac[2]}

exit 0
