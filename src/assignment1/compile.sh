#!/bin/bash

# Compile all packages with in the current directory
find . -type f -name "*_pac.java" -exec javac -d . {} \;

# Compile all classes in the current directory
find . -type f -name "*_cls.java" -exec javac {} \;

# Get arr of classes
cls=($(ls -d *_cls.java | grep -oP '.*?(?=\.)'))
cls_len=${#cls[*]}

# Get arr of packages
pac=($(ls -d *_pac.java | grep -oP '.*?(?=\.)'))
pac_len=${#cls[*]}

# Run all classes
for (( i = 0; i < $cls_len; i++ ))
do
    if [ "$i" = 0 ]; then printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n"; fi
    if [ "$i" != 0 ]; then printf "\n"; fi
    echo "${cls[$i]}.java : "
    java ${cls[$i]}
done

exit 0
