package se.miun.rakr1600.dt062g.jpaint;

import java.util.Scanner;


/**
Assignment 1

This application allows the user to enter data for a circle
or rectangle. The circumference and area are then calculated
and the result is displayed to the standard output.


@author Ragnar Kruse (rakr1600)
@version 1.0
@since 2019-11-05
*/
public class Assignment1
{
    Scanner scanner;

    /** Constructor */
    public Assignment1()
    {
        this.scanner = new Scanner(System.in);

        System.out.println("Enter exit to quit the program." + "\n");

        this.event_loop();
    }

    /** User input
    @return {{-1: 'loop'}, {0: 'exit'}, {1: 'circle'}, {2: 'rectangle'}}
    */
    public int user_input()
    {
        String input = this.scanner.nextLine();

        if (input.equals("exit") || input.equals("0") || input.equals("quit"))
        {
            System.out.println("Good bye!");
            return 0;
        }
        else if (input.equals("circle") || input.equals("1"))
        {
            Circle circle = new Circle();
            return 1;
        }
        else if (input.equals("rectangle") || input.equals("2"))
        {
            Rectangle rectangle = new Rectangle();
            return 2;
        }
        else
        {
            System.out.println("Unknown shape!");
            return -1;
        }
    }

    /** Event loop */
    public void event_loop()
    {
        int exit = -1;

        while (exit != 0)
        {
            System.out.println("\n" + "What geometric shape do you want to use?");

            exit = this.user_input();
        }
    }

    /** Entrypoint */
    public static void main(String[] args)
    {
        Assignment1 p = new Assignment1();
    }
}

/**
This Class containts common functionallity of shapes.
*/
class Shape
{
    /** Member variables */
    static final double PI = 3.14;

    double circumference;
    double area;

    Scanner scanner;
    int num_of_input;

    /** Constructor takes num of times to loop for user input */
    public Shape(int num_of_input)
    {
        this.num_of_input = num_of_input;

        this.scanner = new Scanner(System.in);

        this.input_loop();
    }

    /** Takes all parameters for the class from user input */
    public void input_loop()
    {
        for (int i = 0; i < this.num_of_input; i++)
        {
            String input = "";

            while (input.length() == 0)
            {
                this.params(i, null);
                
                input = scanner.nextLine();

                if (input.length() > 0)
                    this.params(i, input);
            }
        }
    }

    /** Prints or sets the correct params that the input_loop asks for */
    public void params(int i, String input)
    {}

    /** Print shape */
    public void print()
    {
        System.out.println("circumference: " + this.circumference);
        System.out.println("area: " + this.area);
    }
}

/**
This Class encapsulates a circle.
*/
class Circle extends Shape
{
    /** Member variables */
    private double radius;

    /** Constructor */
    public Circle()
    {
        // set num of times to loop for user input
        super(1);

        this.circumference = 2 * this.PI * this.radius;
        this.area = this.PI * (this.radius * this.radius);

        this.print();
    }

    /** Prints or sets the correct params that the input_loop asks for */
    public void params(int i, String input)
    {
        switch (i)
        {
            case 0:
                if (input != null)
                    this.radius = Double.parseDouble(input);
                else
                    System.out.println("Enter radius: ");
                break;
        }
    }
}

/**
This Class encapsulates a rectangle.
*/
class Rectangle extends Shape
{
    /** Member variables */
    private double width;
    private double height;

    /** Constructor */
    public Rectangle()
    {
        // set num of times to loop for user input
        super(2);

        this.circumference = 2 * (this.width + this.height);
        this.area = this.width * this.height;

        this.print();
    }

    /** Prints or sets the correct params that the input_loop asks for */
    public void params(int i, String input)
    {
        switch (i)
        {
            case 0:
                if (input != null)
                    this.width = Double.parseDouble(input);
                else
                    System.out.println("Enter width: ");
                break;
            case 1:
                if (input != null)
                    this.height = Double.parseDouble(input);
                else
                    System.out.println("Enter height: ");
                break;
        }
    }
}
