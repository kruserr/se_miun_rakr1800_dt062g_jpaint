#!/bin/bash

# ls names of all files with .java extension
arr=($(ls -d *_cls.java | grep -oP '.*?(?=\.)'))
arr_len=${#arr[*]}

# Loop through array and run each file individually
for (( i = 0; i < $arr_len; i++ ))
do
    if [ "$i" = 0 ]; then printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n"; fi
    if [ "$i" != 0 ]; then printf "\n"; fi
    echo "${arr[$i]}.java : "
    java ${arr[$i]}
done

exit 0
