#/bin/bash

# Env
production=0

# Container Dev User
container="java_dt062g"
container_user=$(echo $USER)
container_password=$(openssl rand -base64 32)

if [ "$production" != 1 ]; then
    docker container stop $container
fi

docker build -t $container .

if [ "$production" == 1 ]; then
    docker run -dit --restart=always -w /usr/src/myapp --name $container $container
else
    docker run -dit --rm -v $(pwd)/src:/usr/src/myapp -w /usr/src/myapp --name $container $container

    # Add user for dev edit premissions
    docker exec $container /bin/sh -c "useradd -g users -d /home/$container_user -s /bin/bash -p '$container_password' $container_user"
    docker exec $container /bin/sh -c "chown -R $container_user:root /usr/src/myapp"

    docker exec -it $container bash
fi
