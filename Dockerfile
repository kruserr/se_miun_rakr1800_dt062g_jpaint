FROM openjdk:13

COPY src/ /usr/src/myapp
WORKDIR /usr/src/myapp

COPY inc/init.sh /root/init.sh
CMD ["sh", "-c", "/root/init.sh; bash"]
